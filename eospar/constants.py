# Eospar - an first-person adventure game
# Copyright (C) 2018  Chris Murphy and Charlie Murphy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Constant values used by the game.

Attributes:
    FPS (int): The desired frames per second for the game.
    LOGICAL_SCREEN_WIDTH (int):
        The width of the game screen before scaling (in pixels).
    LOGICAL_SCREEN_HEIGHT (int):
        The height of the game screen before scaling (in pixels).
    LOGICAL_SCREEN_SIZE (tuple):
        The dimensions of the game screen before scaling (in pixels).
    SCREEN_SCALE (int): The factor to scale the screen by.
    SCREEN_SIZE (int): The screen size after scaling (in pixels).
    TILE_CLIPS (dict):
        A mapping from tile displacements to their clips for drawing.
"""
FPS = 30
LOGICAL_SCREEN_WIDTH = 256
LOGICAL_SCREEN_HEIGHT = 240
LOGICAL_SCREEN_SIZE = LOGICAL_SCREEN_WIDTH, LOGICAL_SCREEN_HEIGHT
SCREEN_SCALE = 2
SCREEN_SIZE = tuple([v * SCREEN_SCALE for v in LOGICAL_SCREEN_SIZE])
TILE_CLIPS = {
    (-2, -2): {'rect': (0, 0, 128, 240), 'offset': (0, 0)},
    (-1, -2): {'rect': (256, 0, 128, 240), 'offset': (0, 0)},
    ( 0, -2): {'rect': (512, 0, 256, 240), 'offset': (0, 0)},
    ( 1, -2): {'rect': (256, 0, 128, 240), 'offset': (128, 0)},
    ( 2, -2): {'rect': (0, 0, 128, 240), 'offset': (128, 0)},
    (-2, -1): None,
    (-1, -1): {'rect': (768, 0, 128, 240), 'offset': (0, 0)},
    ( 0, -1): {'rect': (0, 240, 256, 240), 'offset': (0, 0)},
    ( 1, -1): {'rect': (768, 0, 128, 240), 'offset': (128, 0)},
    ( 2, -1): None,
    (-2,  0): None,
    (-1,  0): {'rect': (256, 240, 128, 240), 'offset': (0, 0)},
    ( 0,  0): {'rect': (512, 240, 256, 240), 'offset': (0, 0)},
    ( 1,  0): {'rect': (256, 240, 128, 240), 'offset': (128, 0)},
    ( 2,  0): None,
}
