# Eospar - an first-person adventure game
# Copyright (C) 2018  Chris Murphy and Charlie Murphy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Functions for manipulating coordinates."""

def translate(a, b):
    """Return the elementwise sum of two iterables as a tuple."""
    return tuple(map(sum, zip(a, b)))

def scale(scalar, it):
    """Return a tuple made of scaled items from an iterable."""
    return tuple([scalar * x for x in it])
