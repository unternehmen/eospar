# Eospar - an first-person adventure game
# Copyright (C) 2018  Chris Murphy and Charlie Murphy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""The __main__ module contains the entrypoint function."""
import sys
import pygame
import pkg_resources
from eospar import constants, version, direction, geometry

tile_defs = [
    {
        'id': 0,
        'name': 'floor',
        'image': 'data/floor.png',
        'solid': False
    },
    {
        'id': 1,
        'name': 'wall',
        'image': 'data/wall.png',
        'solid': True
    },
    {
        'id': 2,
        'name': 'grass',
        'image': 'data/grass.png',
        'solid': False
    }
]

stage = {
    'width': 10,
    'height': 11,
    'data': [
        [1, 1, 1, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 1, 1, 1, 1, 1, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 1, 1, 0, 1, 0, 1, 0, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 0, 0, 1],
        [1, 0, 1, 0, 1, 0, 1, 1, 0, 1],
        [1, 0, 0, 0, 0, 0, 0, 2, 2, 1],
        [1, 0, 1, 1, 1, 0, 1, 1, 2, 1],
        [1, 0, 1, 0, 1, 0, 1, 1, 2, 1],
        [1, 0, 0, 0, 1, 0, 1, 1, 2, 1],
        [1, 1, 1, 1, 1, 0, 1, 1, 1, 1]
    ]
}

player = {
    'location': (1, 1),
    'direction': direction.SOUTH
}

def load_image(path):
    """Load an image from a resource named by a path."""
    stream = pkg_resources.resource_stream('eospar', path)
    return pygame.image.load(stream)

def get_tile_at(stage, tile_defs, location):
    """Return the definition of the tile at a location on a stage."""
    tid = stage['data'][location[1]][location[0]]
    for tdef in tile_defs:
        if tdef['id'] == tid:
            return tdef

def is_walkable(stage, tile_defs, location):
    """Return whether a location is walkable on a stage."""
    return not get_tile_at(stage, tile_defs, location)['solid']

def is_within_stage_bounds(stage, location):
    """Return whether a location is within the boundaries of a stage."""
    x, y = location
    return x >= 0 and y >= 0 \
           and x < stage['width'] \
           and y < stage['height']

def main():
    """The entrypoint of the game's execution."""
    # Initialize the game state.
    pygame.init()
    pygame.display.set_caption('Eospar ' + version.__version__)
    screen = pygame.display.set_mode(constants.SCREEN_SIZE)
    virtual_screen = pygame.Surface(constants.LOGICAL_SCREEN_SIZE)
    images = {}
    images['sky'] = load_image('data/sky.png')
    clock = pygame.time.Clock()
    # Run the main loop.
    while True:
        # Handle user input.
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    # Try to move the player forward.
                    dest = \
                        geometry.translate(
                            player['location'],
                            direction.to_offset(
                                player['direction']))
                    if is_within_stage_bounds(stage, dest) \
                       and is_walkable(stage, tile_defs, dest):
                        player['location'] = dest
                elif event.key == pygame.K_DOWN:
                    # Try to move the player backward.
                    dest = \
                        geometry.translate(
                            player['location'],
                            geometry.scale(
                                -1,
                                direction.to_offset(
                                    player['direction'])))
                    if is_within_stage_bounds(stage, dest) \
                       and is_walkable(stage, tile_defs, dest):
                        player['location'] = dest
                elif event.key == pygame.K_RIGHT:
                    # Turn the player right.
                    player['direction'] = \
                        direction.right_of(player['direction'])
                elif event.key == pygame.K_LEFT:
                    # Turn the player left.
                    player['direction'] = \
                        direction.left_of(player['direction'])
        # Clear the screen.
        virtual_screen.fill((0, 0, 0))
        # Draw the sky.
        virtual_screen.blit(images['sky'], (0, 0))
        # Draw the tiles from the player's point of view.
        forwardvec = direction.to_offset(
                       player['direction'])
        rightvec = direction.to_offset(
                     direction.right_of(
                       player['direction']))

        for v in range(-2, 1):
            for u in range(-2, 1):
                if u == 0:
                    pairs = [(u, v)]
                else:
                    pairs = [(u, v), (-u, v)]
                for pu, pv in pairs:
                    displacement = geometry.translate(
                                       geometry.scale(pu, rightvec),
                                       geometry.scale(-pv, forwardvec))
                    location = geometry.translate(player['location'],
                                                  displacement)
                    if is_within_stage_bounds(stage, location):
                        # Determine what kind of tile is being drawn.
                        tdef = get_tile_at(stage, tile_defs, location)
                        if tdef['image']:
                            # Load the tile's image if necessary.
                            if tdef['image'] not in images:
                                images[tdef['image']] = \
                                    load_image(tdef['image'])
                            # Draw the tile.
                            clip = constants.TILE_CLIPS[(pu, pv)]
                            if clip:
                                virtual_screen.blit(
                                    images[tdef['image']],
                                    clip['offset'],
                                    geometry.translate(
                                        clip['rect'],
                                        clip['offset'] + (0, 0)))

        # Scale up the screen.
        pygame.transform.scale(virtual_screen,
                               constants.SCREEN_SIZE,
                               screen)
        # Update the display.
        pygame.display.flip()
        # Delay until the next frame.
        clock.tick(constants.FPS)

if __name__ == '__main__':
    main()
