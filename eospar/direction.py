# Eospar - an first-person adventure game
# Copyright (C) 2018  Chris Murphy and Charlie Murphy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""Functions for dealing with directions and offsets.

Attributes:
    NORTH (int): the direction north
    EAST (int): the direction east
    SOUTH (int): the direction south
    WEST (int): the direction west
"""
NORTH = 0
EAST = 1
SOUTH = 2
WEST = 3

def right_of(direction):
    """Return the direction to the right of the given direction."""
    if direction == NORTH:
        return EAST
    if direction == EAST:
        return SOUTH
    if direction == SOUTH:
        return WEST
    if direction == WEST:
        return NORTH
    raise ValueError('unknown direction: %d' % (direction,))

def left_of(direction):
    """Return the direction to the left of the given direction."""
    if direction == NORTH:
        return WEST
    if direction == EAST:
        return NORTH
    if direction == SOUTH:
        return EAST
    if direction == WEST:
        return SOUTH
    raise ValueError('unknown direction: %d' % (direction,))

def to_offset(direction):
    """Convert a direction into a pair of offset coordinates."""
    if direction == NORTH:
        return 0, -1
    if direction == EAST:
        return 1, 0
    if direction == SOUTH:
        return 0, 1
    if direction == WEST:
        return -1, 0
    raise ValueError('unknown direction: %d' % (direction,))
