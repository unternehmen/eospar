# Eospar - an first-person adventure game
# Copyright (C) 2018  Chris Murphy and Charlie Murphy
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup, find_packages

version = {}
with open('eospar/version.py') as fp:
    exec(fp.read(), version)

setup(
    name='eospar',
    version=version['__version__'],
    packages=find_packages(),
    author='Chris Murphy, Charlie Murphy',
    author_email='eospar-development@googlegroups.com',
    license='GPLv3+',
    install_requires=['pygame==1.9.4'],
    zip_safe=False,
    include_package_data=True,
    entry_points={
        'gui_scripts': [
            'eospar = eospar.__main__:main'
        ]
    }
)
