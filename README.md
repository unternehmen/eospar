# Eospar

## Introduction
Eospar is a first-person adventure game and the successor to an
unfinished game called [dirt](https://github.com/unternehmen/dirt).
It is written in [Python 3](https://www.python.org/) and uses the
[Pygame](https://www.pygame.org/) library.

## Installation
To install Eospar straight from the online repository, use this
command:

    pip install git+https://gitlab.com/unternehmen/eospar.git

If you have already downloaded Eospar onto your computer, you can
install it by navigating to the source directory and using this
command:

    pip install .

Lastly, if you are developing Eospar and wish to make changes without
needing to reinstall it every time, you can install it with this
command:

    pip install -e .

## Running
After Eospar is installed, you can run it using this command:

    eospar

## License
Copyright (C) 2018  Chris Murphy and Charlie Murphy

Eospar is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
